package javaoop.task2;

public abstract class Calculator {

    public abstract int add(int a, int b);

    public abstract int subtract(int a, int b);

    public abstract int multiply(int a, int b);

    public abstract float divide(int a, int b);

}


