package javajunit;

import javaoop.task2.NumberCalculator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CalculatorTest {
    private NumberCalculator calculator;

    @Before
    public void setUp() {
        this.calculator = new NumberCalculator();
    }

    @Test
    public void shouldAddTwoIntsTest() {
        int result = calculator.add(1, 2);
        Assert.assertEquals(result, 3);
    }

    @Test
    public void shouldAddTwoDoublesTest() {
        double result = calculator.add(1.5, 2.1);
        Assert.assertEquals(result, 3.6, 0.0001);
    }

    @Test
    public void shouldMinusTwoIntsTest() {
        int result = calculator.subtract(8, 3);
        Assert.assertEquals(result, 5);
    }

    @Test
    public void shouldMinusTwoDoublesTest() {
        double result = calculator.subtract(8.3, 3.0);
        Assert.assertEquals(result, 5.3, 0.001);
    }

    @Test
    public void shouldMultiplyTwoIntsTest() {
        int result = calculator.multiply(2, 5);
        Assert.assertEquals(result, 10);
    }

    @Test
    public void shouldMultiplyTwoDoublesTest() {
        double result = calculator.multiply(2, 5.5);
        Assert.assertEquals(result, 11, 0);
    }

    @Test
    public void shouldDevideTwoIntsTest() {
        float result = calculator.divide(6, 2);
        Assert.assertEquals(result, 2.0, 0);
    }

    @Test
    public void shouldDevideTwoDoublesTest() {
        double result = calculator.divide(6.2, 3.1);
        Assert.assertEquals(result, 2.0, 0);
    }
}
