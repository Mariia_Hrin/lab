package javaoop.task2;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class NumberCalculator extends Calculator {
    private static final Logger Log = LogManager.getLogger(NumberCalculator.class);

    public double add(double a, double b) {
        Log.debug("Add operation started with arguments:" + a + ", " + b);
        double result = a + b;
        Log.info("Result is: " + result);
        return result;
    }

    public double subtract(double a, double b) {
        Log.debug("Subtract operation started with arguments:" + a + ", " + b);
        double result = a - b;
        Log.info("Result is: " + result);
        return result;
    }

    public double multiply(double a, double b) {
        Log.debug("Multiply operation started with arguments:" + a + ", " + b);
        double result = a * b;
        Log.info("Result is: " + result);
        return result;
    }

    public double divide(double a, double b) {
        if (b == 0) {
            Log.error("Divide by zero impossible");
            throw new ArithmeticException("Division by zero");
        } else {
            Log.debug("Divide operation started with arguments:" + a + ", " + b);
            double result = a / b;
            Log.info("Result is: " + result);
            return result;
        }
    }

    public int add(int a, int b) {
        Log.debug("Add operation started with arguments:" + a + ", " + b);
        int result = a + b;
        Log.info("Result is: " + result);
        return result;
    }

    public int subtract(int a, int b) {
        Log.debug("Subtract operation started with arguments:" + a + ", " + b);
        int result = a - b;
        Log.info("Result is: " + result);
        return result;
    }

    public int multiply(int a, int b) {
        Log.debug("Multiply operation started with arguments:" + a + ", " + b);
        int result = a * b;
        Log.info("Result is: " + result);
        return result;
    }

    public float divide(int a, int b) {
        if (b == 0) {
            Log.error("Divide by zero impossible");
            throw new ArithmeticException("Division by zero");
        } else {
            Log.debug("Divide operation started with arguments:" + a + ", " + b);
            int result = a / b;
            Log.info("Result is: " + result);
            return result;
        }
    }

}
