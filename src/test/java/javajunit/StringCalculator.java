package javajunit;

public class StringCalculator {
    public int add(String numbers) throws IllegalArgumentException {
        int result = 0;
        if (numbers == "") {
            return 0;
        }
        String delimiters = ",|\n";
        if (numbers.contains("//")) {
            delimiters = "[" + numbers.substring(2, numbers.indexOf("\n")) + "]";
            numbers = numbers.substring(numbers.indexOf("\n") + 1);
        }
        String errorMessage = "Negatives not allowed:";
        String negatives = "";

        String[] numbersStr = numbers.split(delimiters);
        for (String str : numbersStr) {
            if (str.length() == 0) {
                continue;
            }
            int number = Integer.parseInt(str);
            if (number < 0) {
                negatives += number;
            }
            if (number < 1000) {
                result += number;
            }
        }
        if (negatives != "") {
            throw new IllegalArgumentException(errorMessage + negatives);
        }

        return result;
    }
}
