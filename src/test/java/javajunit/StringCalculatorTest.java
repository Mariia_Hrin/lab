package javajunit;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class StringCalculatorTest {

    private StringCalculator stringCalculator;

    @Before
    public void setUp() {
        this.stringCalculator = new StringCalculator();
    }

    @Test
    public void shouldReturnZeroWhenEmptyStringProvidedTest() throws Exception {
        int result = this.stringCalculator.add("");
        Assert.assertEquals(result, 0);
    }

    @Test
    public void shouldReturnSameDigitWhenOneDigitStringProvidedTest() throws Exception {
        int result = this.stringCalculator.add("3");
        Assert.assertEquals(result, 3);
    }

    @Test
    public void shouldReturnSumOfDigitsWhenTwoDigitStringsProvidedTest() throws Exception {
        int result = this.stringCalculator.add("3,4");
        Assert.assertEquals(result, 7);
    }

    @Test
    public void shouldReturnSumOfUknownAmountProvidedTest() throws Exception {
        int result = this.stringCalculator.add("3,4,5,6");
        Assert.assertEquals(result, 18);
    }

    @Test
    public void shouldReturnSumOfUknownAmountSeparatedWiNewLinesTest() throws Exception {
        int result = this.stringCalculator.add("3\n4\n5,6");
        Assert.assertEquals(result, 18);
    }

    @Test
    public void shouldSupportDiferentDelimitersTest() throws Exception {
        int result = this.stringCalculator.add("//;\n1;2");
        Assert.assertEquals(result, 3);
    }

    @Test
    public void shouldThrowExceptionsForNegativesTest() throws Exception {
        try {
            this.stringCalculator.add("//;\n1;2;-10;-20");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(e.getMessage(), "Negatives not allowed:-10-20");
        }
    }

    @Test
    public void shouldIgnoreNumbersMore1000Test() throws Exception {
        int result = this.stringCalculator.add("//;\n2;1000");
        Assert.assertEquals(result, 2);
    }

    @Test
    public void shouldSupportDelimitersWithAnyLengthTest() throws Exception {
        int result = this.stringCalculator.add("//***\n1***2***3");
        Assert.assertEquals(result, 6);
    }

    @Test
    public void shouldSupportMultipleDelimitersTest() throws Exception {
        int result = this.stringCalculator.add("//[*][%]\n10*2%3");
        Assert.assertEquals(result, 15);
    }

    @Test
    public void shouldSupportMultipleDelimitersWithAnyLengthTest() throws Exception {
        int result = this.stringCalculator.add("//[***][%]\n1***2%3");
        Assert.assertEquals(result, 6);
    }
}
