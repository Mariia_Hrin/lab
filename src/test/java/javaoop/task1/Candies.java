package javaoop.task1;

public class Candies extends Sweet {
    private String candyTaste;

    public Candies(String name, double price, int weight, String candyTaste) throws Exception {
        super(name, price, weight);
        this.candyTaste = candyTaste;
    }

    public String getFilling() {
        return candyTaste;
    }

    @Override
    public String toString() {
        return this.name + "(" + this.candyTaste + "), " + this.weight +
                "g, " + this.price + " grn";
    }
}
