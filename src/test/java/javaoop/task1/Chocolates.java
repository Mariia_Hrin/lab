package javaoop.task1;

public class Chocolates extends Sweet {
    private String typeChocolate;

    public Chocolates(String name, double price, int weight, String typeChocolate) throws Exception {
        super(name, price, weight);
        this.typeChocolate = typeChocolate;
    }

    public String getType() {
        return typeChocolate;
    }

    @Override
    public String toString() {
        return this.name + "(" + this.typeChocolate + "), " + this.weight +
                "g, " + this.price + " grn";
    }
}
