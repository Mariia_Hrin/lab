package javaoop.task1;

public class Cookies extends Sweet {
    private int count;

    public Cookies(String name, double price, int weight, int count) throws Exception {
        super(name, price, weight);
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    @Override
    public String toString() {
        return this.name + "(" + this.count + " pieces)," + this.weight +
                "g, " + this.price + " grn";
    }

}
