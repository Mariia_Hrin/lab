package javaoop.task1;

import java.util.Arrays;
import java.util.Comparator;

public class Giftbox {

    protected double giftWeight = 0;
    private Sweet[] sweets;

    public Giftbox(Sweet[] sweets) {
        this.sweets = sweets;
    }

    public void showSweets() {
        for (int i = 0; i < this.sweets.length; i++) {
            System.out.println(this.sweets[i].toString());
        }
    }

    public double getGiftWeight() {
        for (int i = 0; i < sweets.length; i++) {
            giftWeight += sweets[i].getWeight();
        }
        System.out.println("The weight of giftbox is " + giftWeight + "g");
        return giftWeight;
    }

    public void sortByWeight() {
        Arrays.sort(this.sweets, new Giftbox.SortByWeight());
    }

    public void findSweet(String name) {
        for (int i = 0; i < sweets.length; i++) {
            if (name == sweets[i].getName()) {
                System.out.println("Seaching sweet: " + this.sweets[i]);
                return;
            }
        }
        System.out.println("Sorry, we can't find this sweet. Please, check the name");
    }

    static class SortByWeight implements Comparator<Sweet> {
        public int compare(Sweet a, Sweet b) {
            return (int) (a.getWeight() - b.getWeight());
        }
    }
}
