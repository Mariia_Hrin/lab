package javaoop.task1;

import java.util.Arrays;

public class Runner {

    public static void main(String[] args) throws Exception {
        Sweet[] sweets = {
                new Chocolates("Milka", 27.50, 125, "milk"),
                new Candies("Chupachups", 10.80, 10, "cherry"),
                new Cookies("Oreo", 15.60, 180, 15),
                new Chocolates("Millenium", 35.40, 230, "white"),
                new Chocolates("Korona", 15.25, 140, "milk"),
                new Candies("Nuts", 10.20, 80, "hazelnut"),
                new Cookies("Hit", 47.50, 220, 10),
                new Candies("Raffaelo", 60.20, 320, "coconut")
        };
        Giftbox giftbox = new Giftbox(sweets);

        System.out.println("Sweets in the giftbox:");
        giftbox.showSweets();

        System.out.println("");
        giftbox.getGiftWeight();

        giftbox.sortByWeight();
        System.out.println("\nSorted by weight:");
        giftbox.showSweets();

        System.out.println("");
        giftbox.findSweet("Raffaelo");
    }
}
