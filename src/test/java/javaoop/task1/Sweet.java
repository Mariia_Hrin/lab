package javaoop.task1;

public abstract class Sweet {

    protected String name;
    protected int weight;
    protected double price;

    public Sweet(String name, double price, int weight) throws Exception {
        if ((price < 0) || (weight < 0)) {
            throw new Exception("You should provide positive price and weight");
        }
        this.name = name;
        this.price = price;
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public double getWeight() {
        return weight;
    }

    @Override
    public String toString() {
        return this.name + ", " + this.weight +
                "g, " + this.price + " grn";
    }
}
