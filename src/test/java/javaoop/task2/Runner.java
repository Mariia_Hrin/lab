package javaoop.task2;

import java.util.Scanner;

public class Runner {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the first number:");
        double a = scanner.nextDouble();
        System.out.println("Enter the second number:");
        double b = scanner.nextDouble();
        System.out.println("Enter the operation(+,-,*,/):");
        char operation = scanner.next(".").charAt(0);
        NumberCalculator calculator = new NumberCalculator();

        switch (operation) {
            case '+':
                System.out.println("Result: " + calculator.add(a, b));
                break;
            case '-':
                System.out.println("Result: " + calculator.subtract(a, b));
                break;
            case '*':
                System.out.println("Result: " + calculator.multiply(a, b));
                break;
            case '/':
                System.out.println("Result: " + calculator.divide(a, b));
                break;
            default:
                System.out.println("Something went wrong. Please check the operation.");
        }
    }
}
